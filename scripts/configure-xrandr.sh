#!/bin/bash -e

PRIMARY="eDP"
EXT1="DisplayPort-2"
#EXT2="VGA-0"

if (xrandr | grep "$EXT1 connected"); then
    xrandr --output $EXT1 --primary --auto --scale 1.5x1.5 --output $EXT1 --auto --right-of $PRIMARY
    echo "Docking connected, increasing scale"
#elif (xrandr | grep "$EXT2 connected"); then
#    xrandr --output $EXT2 --off
#    xrandr --output $PRIMARY --primary --auto --output $EXT2 --auto --right-of $PRIMARY
#    echo "second screen (VGA) enabled"
else
#    xrandr --output $EXT1 --off
#    xrandr --output $EXT2 --off
#    xrandr --output $PRIMARY --primary --auto
    echo "Docking not connected"
fi
