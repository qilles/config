# Dotfiles
## Configs
- I3
- Polybar
- Kitty
- NVim
- Ranger

## Dependencies
### Packages
- Kitty
- NeoVIM

### Fonts
- JetBrains Mono Nerd Font
- Material Design Icons Desktop
- FontAwesome 5 Pro

### Current devices
- Macbook Pro 2018 running Ubuntu 22.04 using T2 Linux
- HP Elitebook running Ubuntu 20.04, connecting trough NoMachine

### Specifics
- Clone the dots using YADM (Yet Another Dotfiles Manager)
- I'm using X11 because Pavucontrol is having issues using Wayland.
- I currently see no need in using PICOM, keeping it simple, stupid.
- I'm debating on moving to Arch for rolling releases for packages, LTS is a bit cumbersome. (Dunst is out of date, i3 using old packages and cba building from source)
- Xrandr config is set using Work from home setup, Zwijnaarde config needs to be created
- Configure the .zshrc to set aliases based on which device I'm using.
- Kitty is having sudo issues, these can be solved by copying the TERMINFO file into /etc/
```
sudo cp -a $TERMINFO/. /etc/terminfo/ 
```
- NoMachine uses X11 protocol and connects to a virtual screen, make sure the license is installed. 
